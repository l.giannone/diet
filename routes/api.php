<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DietController;
use App\Http\Controllers\WeekController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/dates', [WeekController::class, 'dates'])->name('dates');
Route::get('/diet', [DietController::class, 'diet'])->name('diet');
Route::get('/day', [DietController::class, 'day'])->name('day');
Route::put('/dietdate/{ceck}', [DietController::class, 'dietdate'])->name('dietdate');

Route::post('/insert', [DietController::class, 'insert'])->name('insert');
Route::put('/edit/{diet}', [DietController::class, 'edit'])->name('edit');