import { createRouter, createWebHistory } from "vue-router";
import Week from '../week/week.vue';
import Dasboard from '../dasboard/dasboard.vue';



const routes = [
    { path: '/week', 
      name: 'dashboard',
      component: Dasboard,
    },
    { path: '/', 
      name: 'week',
      component: Week,
    },
  ]

  export default createRouter({
    history : createWebHistory(),
    routes, 
  
  })