<?php

use App\Models\Week;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('weeks', function (Blueprint $table) {
            $table->unsignedBigInteger('ceck_id')->nullable();
            $table->foreign('ceck_id')->references('id')->on('cecks')->onDelete('cascade');
        });


        if("Sunday" == date('l',strtotime(date('Y-01-01')))){
         
            $NewDate = date('Y-m-d', strtotime(date('Y-01-01'). ' + 1 days'));
            $weeks = (365 - 1) / 7;
            $week = 0;
            $week2 = 6;
            
            for ($i = 1; $i <=  $weeks; $i++) {
                $z = new Week();
                $z->first = date('Y-m-d', strtotime(date("{$NewDate}"). " + {$week} days"));
                $z->second = date('Y-m-d', strtotime(date("{$NewDate}"). " + {$week2} days"));
                $z->ceck_id = $i;
                $z->save();
                $week += 7;
                $week2 += 7;
                
            };     
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('weeks', function (Blueprint $table) {
            $table->dropForeign(['ceck_id']);
            $table->dropColumn('ceck_id');
        });
    }
};
