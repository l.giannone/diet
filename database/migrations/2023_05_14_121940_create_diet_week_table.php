<?php

use App\Models\Diet;
use App\Models\Week;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('diet_week', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('diet_id')->nullable();
            $table->foreign('diet_id')->references('id')->on('diets')->onDelete('cascade');
            $table->unsignedBigInteger('week_id')->nullable();
            $table->foreign('week_id')->references('id')->on('weeks')->onDelete('cascade');
            $table->timestamps();
        });
      
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('diet_week');
        
    }
};
