<?php

use App\Models\Ceck;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('cecks', function (Blueprint $table) {
            $table->id();
            $table->integer('a')->default(1);
            $table->integer('b')->default(1);
            $table->integer('c')->default(1);
            $table->integer('d')->default(1);
            $table->integer('e')->default(1);
            $table->integer('f')->default(1);
            $table->integer('g')->default(1);
            $table->integer('h')->default(1);
            $table->integer('i')->default(1);
            $table->integer('l')->default(1);
            $table->integer('m')->default(1);
            $table->integer('n')->default(1);
            $table->integer('o')->default(1);
            $table->integer('p')->default(1);
            $table->timestamps();
        });

        if("Sunday" == date('l',strtotime(date('Y-01-01')))){
         
            $weeks = (365 - 1) / 7;
           
            for ($i = 1; $i <=  $weeks; $i++) {
                $z = new Ceck();
                $z->save();
            };     
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('cecks');
    }
};
