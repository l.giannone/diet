<?php
use App\Models\Diet;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('diets', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('lunch');
            $table->string('descrptionlunch');
            $table->timestamps();
        });
        
        /* $repeats = ["Lunedì", 
        "Lunedì",
        'Martedì', 
        'Martedì',
        'Mercoledì', 
        'Mercoledì',
        'Giovedì', 
        'Giovedì','Venerdì', 'Venerdì','Sabato','Sabato','Domenica','Domenica']; */
        /*  $repeats = array(
        "1" =>array(
            "title"=>"Lunedì",
            "dinner"=> "Pranzo",
            "description"=> "Descrption lunch, Descrption lunch , Descrption lunch, Descrption lunch, Descrption lunch, Descrption lunch, Descrption lunch",
        ),
        "2" =>array(
            "title"=>"Lunedì",
            "dinner"=> "Cena",
            "description"=> "Descrption lunch, Descrption lunch , Descrption lunch, Descrption lunch, Descrption lunch, Descrption lunch, Descrption lunch",
        ),
        "3" =>array(
            "title"=>"Martedì",
            "dinner"=> "Pranzo",
            "description"=> "Descrption lunch, Descrption lunch , Descrption lunch, Descrption lunch, Descrption lunch, Descrption lunch, Descrption lunch",
        ),
        "4" =>array(
            "title"=>"Martedì",
            "dinner"=> "Cena",
            "description"=> "Descrption lunch, Descrption lunch , Descrption lunch, Descrption lunch, Descrption lunch, Descrption lunch, Descrption lunch",
        ),
        "5" =>array(
            "title"=>"Mercoledì",
            "dinner"=> "Pranzo",
            "description"=> "Descrption lunch, Descrption lunch , Descrption lunch, Descrption lunch, Descrption lunch, Descrption lunch, Descrption lunch",
        ),
        "6" =>array(
            "title"=>"Mercoledì",
            "dinner"=> "Cena",
            "description"=> "Descrption lunch, Descrption lunch , Descrption lunch, Descrption lunch, Descrption lunch, Descrption lunch, Descrption lunch",
        ),
        "7" =>array(
            "title"=>"Giovedì",
            "dinner"=> "Pranzo",
            "description"=> "Descrption lunch, Descrption lunch , Descrption lunch, Descrption lunch, Descrption lunch, Descrption lunch, Descrption lunch",
        ),
        "8" =>array(
            "title"=>"Giovedì",
            "dinner"=> "Cena",
            "description"=> "Descrption lunch, Descrption lunch , Descrption lunch, Descrption lunch, Descrption lunch, Descrption lunch, Descrption lunch",
        ),
        "9" =>array(
            "title"=>"Venerdì",
            "dinner"=> "Pranzo",
            "description"=> "Descrption lunch, Descrption lunch , Descrption lunch, Descrption lunch, Descrption lunch, Descrption lunch, Descrption lunch",
        ),
        "10" =>array(
            "title"=>"Venerdì",
            "dinner"=> "Cena",
            "description"=> "Descrption lunch, Descrption lunch , Descrption lunch, Descrption lunch, Descrption lunch, Descrption lunch, Descrption lunch",
        ),
        "11" =>array(
            "title"=>"Sabato",
            "dinner"=> "Pranzo",
            "description"=> "Descrption lunch, Descrption lunch , Descrption lunch, Descrption lunch, Descrption lunch, Descrption lunch, Descrption lunch",
        ),
        "12" =>array(
            "title"=>"Sabato",
            "dinner"=> "Cena",
            "description"=> "Descrption lunch, Descrption lunch , Descrption lunch, Descrption lunch, Descrption lunch, Descrption lunch, Descrption lunch",
        ),
        "13" =>array(
            "title"=>"Domenica",
            "dinner"=> "Pranzo",
            "description"=> "Descrption lunch, Descrption lunch , Descrption lunch, Descrption lunch, Descrption lunch, Descrption lunch, Descrption lunch",
        ),
        "14" =>array(
            "title"=>"Domenica",
            "dinner"=> "Cena",
            "description"=> "Descrption lunch, Descrption lunch , Descrption lunch, Descrption lunch, Descrption lunch, Descrption lunch, Descrption lunch",
        ),
    ); */

        
   /*      $diet_identifier = 1;
        foreach ($repeats as $repeat){
            $z = new Diet();
            $z->title = $repeat;
            $z->lunch = "Lunch";
            $z->descrptionlunch = "Descrption lunch, Descrption lunch , Descrption lunch, Descrption lunch, Descrption lunch, Descrption lunch, Descrption lunch";
            $z->save();   

            $diet_identifier += 1;
        } */

    }
    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('diets');
    }
};
