include ./.env
export

help: ## Show this help.
	@echo "Usage: make [options] [target] ..."; \
	echo "Targets:"; \
	fgrep -h '##' Makefile | awk -F'##|: ' '{printf "%40s %s\n", $$1, $$3}' | fgrep -v 'fgrep';

freshdb: ## reset db

	@echo "Reset DB"
	@php artisan migrate:fresh && php artisan passport:install --force

dev: ## serve for development

	php artisan serve &
	npm run dev

install: ##performs initial setup

	@echo "Installing Libreies"
	@composer install && cp .env.example .env &
	@npm install
