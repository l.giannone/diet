<?php

namespace App\Http\Controllers;

use App\Models\Week;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Date;

class WeekController extends Controller
{
    public function dates(){
       
        $dates = Week::all();
       
     
        return response()->json([
            "date"=> $dates,
        ], 200);
    }
}
