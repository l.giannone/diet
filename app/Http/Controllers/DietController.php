<?php

namespace App\Http\Controllers;

use App\Models\Ceck;
use App\Models\Count;
use App\Models\Day;
use App\Models\Diet;
use App\Models\Week;
use Illuminate\Http\Request;

class DietController extends Controller
{
    public function diet(){
 
    if(Count::all()->first() == null){
        foreach(Week::all() as $week){
            foreach(Diet::all() as $diet){
                $diet->weeks()->attach($week);  
            };
        }
    } 

    $dietgiournal = Week::with('diets', 'ceck')->get();

    $z = new Count();
    $z->count = 1;
    $z->save();

    return response()->json([
            "date"=> $dietgiournal,
        ], 200);
    }

    public function insert(Request $request ){
        if(count(Diet::all()->where('day_id' , $request->day)) < 2){
            $z = new Diet();
            $z->title = $request->title;
            $z->lunch = $request->lunch;
            $z->descrptionlunch = $request->description;
            $z->day_id = $request->day;
            $z->save(); 

            foreach(Week::all() as $week){
                $z->weeks()->attach($week);  
            }
        }else{
            return response()->json([
                "response"=>  1,
            ], 200);
        }
        
    }

    public function edit(Request $request , Diet $diet){
    
        
        $diet->title = $request->title;
        $diet->lunch = $request->lunch;
        $diet->descrptionlunch = $request->description;
        $diet->save();

        $dietgiournal = Week::with('diets', 'ceck')->get();
        return response()->json([
            "date"=> $dietgiournal,
        ], 200);


    }

    public function day(){
        $days = Day::all();

    return response()->json([
            "days"=>  $days,
        ], 200);
    }

    public function dietdate(Request $request , Ceck $ceck){
    
        if($request->number == 1){
            $ceck->a = 0;
        } elseif($request->number == 2){
            $ceck->b = 0;
        }elseif($request->number == 3){
            $ceck->c = 0;
        }elseif($request->number == 4){
            $ceck->d = 0;
        }elseif($request->number == 5){
            $ceck->e = 0;
        }elseif($request->number == 6){
            $ceck->f = 0;
        }elseif($request->number == 7){
            $ceck->g = 0;
        }elseif($request->number == 8){
            $ceck->h = 0;
        }elseif($request->number == 9){
            $ceck->i = 0;
        }elseif($request->number == 10){
            $ceck->l = 0;
        }elseif($request->number == 11){
            $ceck->m = 0;
        }elseif($request->number == 12){
            $ceck->n = 0;
        }elseif($request->number == 13){
            $ceck->o = 0;
        }elseif($request->number == 14){
            $ceck->p = 0;
        };
        $ceck->save();
        $dietgiournal = Week::with('diets', 'ceck')->get();
    return response()->json([
            "date"=> $dietgiournal,
        ], 200);
    }
}
