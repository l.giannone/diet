<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Week extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function diets(){
        return $this->belongsToMany(Diet::class);
    }

    public function ceck(){
        return $this->belongsTo(Ceck::class);
    }
    
}
