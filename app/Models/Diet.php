<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Diet extends Model
{
    use HasFactory;


    protected $guarded = [];
    public function weeks(){
        return $this->belongsToMany(Week::class);
    }

    public function days(){
        return $this->belongsTo(Day::class);
    }
    
}
