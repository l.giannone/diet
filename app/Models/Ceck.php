<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ceck extends Model
{
    protected $guarded = [];
    
    use HasFactory;

    public function week(){
        return $this->hasOne(Week::class); 
    }
}
